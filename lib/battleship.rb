require_relative "player"
require_relative "board"

class BattleshipGame
  attr_reader :board, :player

  def initialize
    @player = HumanPlayer.new
    @board = Board.new
  end

  def play
    until @board.won?
      input = @player.get_input
      @board.shoot(input)
    end
  end
end

if __FILE__ == $PROGRAM_NAME
  game = BattleshipGame.new
  game.play
end
