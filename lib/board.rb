class Board
  attr_accessor :grid
  SHIP_UNHIT = :s
  SHIP_HIT = :x
  SHIP_MISS = :o

  def initialize(grid = Board.default_grid)
    @grid = grid
    populate_grid
  end

  def self.default_grid
    grid = []
    10.times { grid << [nil] * 10 }
    grid
  end

  def populate_grid
    chance_to_populate = 25
    @grid.each.with_index do |row, r_idx|
      row.each_index do |c_idx|
        if (rand * 100 + 1).floor <= chance_to_populate
          @grid[r_idx][c_idx] = SHIP_UNHIT
        end
      end
    end
  end

  def shoot(coords)
    if @grid[coords.first][coords.last] == SHIP_UNHIT
      puts "Hit!"
      @grid[coords.first][coords.last] = SHIP_HIT
    elsif @grid[coords.first][coords.last].nil?
      puts "Miss!"
      @grid[coords.first][coords.last] = SHIP_MISS
    else
      puts "You have already shot there"
    end

    display
  end

  def display
    puts "    0 1 2 3 4 5 6 7 8 9"
    puts "  +--------------------"
    @grid.each.with_index do |row, r_idx|
      row_string = "#{r_idx} | "
      row.each do |cell|
        row_string << get_display_char(cell)
      end
      puts row_string
    end
  end

  def count
    @grid.flatten.count(SHIP_UNHIT)
  end

  def empty?(coords = [])
    if coords.empty?
      return count == 0
    end
    @grid[coords.first][coords.last].nil?
  end

  def won?
    count == 0
  end

  def full?
    @grid.flatten.all? { |cell| cell == SHIP_UNHIT }
  end

  private

  def get_display_char(cell)
    if cell.nil? || cell == SHIP_UNHIT # undamaged ship
      "  "
    elsif cell == :x # hit
      "X "
    else
      "O " # miss
    end
  end

end
