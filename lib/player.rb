class HumanPlayer

  def initialize
  end

  def get_input
    print "Input coordinates: "
    input = gets.chomp.split(",")
    [input[0].to_i, input[1].to_i]
  end

end
